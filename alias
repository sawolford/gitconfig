[alias]
	# git
	ruser = !git config --local --unset user.name && git config --local --unset user.email
	hookspath = !git config --local core.hooksPath "~/gitconfig/hooks"
	# helper
	hashempty = !echo 4b825dc642cb6eb9a060e54bf8d69288fbee4904
	root = !git rev-parse --show-toplevel
	broot = !git rev-parse --show-toplevel | xargs -0 basename | head -1
	superroot = !git rev-parse --absolute-git-dir | sed 's,/.git.*,,'
	firsthash = !git rev-list --abbrev-commit HEAD | tail -n 1
	ref = "!f() { git symbolic-ref HEAD | cut -d/ -f3-; }; f"
	locname = "!f() { git name-rev --name-only --exclude='tags/*' --exclude='remotes/*' ${1:-HEAD}; }; f"
	remname = "!f() { git name-rev --name-only --refs='remotes/*' --exclude='remotes/*/HEAD' ${1:-HEAD}; }; f"
	name = "!f() { git rev-parse --abbrev-ref ${1:-HEAD}; }; f"
	lname = "!f() { git rev-parse ${1:-HEAD}; }; f"
	brname = "!f() { git name \"$@\" | grep -v \"^master$\"; }; f"
	branched = "!f() { [ \"$(git name)\" != \"master\" ] && git broot; }; f"
	branchedname = "!f() { [ \"$(git name)\" != \"master\" ] && echo $(git broot)=$(git brname); }; f"
	jsontool = "!f() { $(git varvalue python) -m json.tool \"$@\"; }; f"
	sh = "!f() { \"$@\"; }; f"
	varset = "!f() { git config variable.$1 >/dev/null 2>&1; }; f"
	varvalue = "!f() { echo $(git config variable.$1); }; f"
	vartrue = "!f() { $(git config variable.$1); }; f"
	varfalse = "!f() { ! $(git config variable.$1); }; f"
	# alias
	al = "!f() { git config --list | grep alias | sed \"s,;.*,,\"; }; f"
	alg = "!f() { git al | grep ${1:-.}; }; f"
	# attributes
	lsca = !git ls-files | git check-attr --stdin
	lscaunspecified = "!f() { git lsca ${1:-text} | grep unspecified; }; f"
	reco = !git rm -r . && git reset --hard
	notasciilist = !git mimetype -v us-ascii | sed 's,:.*,,'
	# branch
	resetupstream = "!f() { git branch --set-upstream-to=origin/$(git brname) $(git brname); }; f"
	blog = log --left-right
	br = branch
	bra = branch -a
	brf = branch -f
	brd = branch -d
	brm = branch -m
	bru = branch -u
	brsc = branch --show-current
	brcontains = branch --contains
	upstream = "!f() { git rev-parse --abbrev-ref ${1:-}@{u}; }; f"
	brupstream = "!f() { git upstream | grep -v \"^origin/master$\"; }; f"
	branchedupstream = "!f() { [ \"$(git upstream)\" != \"origin/master\" ] && echo $(git broot)=$(git brupstream); }; f"
	# diff
	di = diff
	dis = diff --staged
	dtt = difftool -y -d
	dttd = difftool -y
	dtk = difftool -y -d --tool=kdiff3
	dtkd = difftool -y --tool=kdiff3
	dtm = difftool -y -d --tool=meld
	dtmd = difftool -y --tool=meld
	dti = difftool -y --tool=icdiff
	dtv = difftool -y --tool=vimdiff
	dts = difftool -y -d --staged
	dtsd = difftool -y --staged
	dtks = difftool -y -d --tool=kdiff3 --staged
	dtksd = difftool -y --tool=kdiff3 --staged
	dtms = difftool -y -d --tool=meld --staged
	dtmsd = difftool -y --tool=meld --staged
	dtis = difftool -y --tool=icdiff --staged
	dtvs = difftool -y --tool=vimdiff --staged
	dii = !git di @{u}..@
	dio = !git di @..@{u}
	dtii = !git dti @{u}..@
	dtio = !git dti @..@{u}
	dtti = !git dtt @{u}..@
	dtto = !git dtt @..@{u}
	# log
	logv = log --stat --dirstat --summary --graph --date=local --pretty=fuller --show-signature
	gl = log --graph --abbrev-commit --left-right --pretty=pretty
	gm = log --reverse --abbrev-commit --first-parent --pretty=minimal
	gla = !git gl --all
	glr = !git gl --all --reflog
	glv = log --name-status
	glva = !git glv --all --reflog
	glil = !git gl ..@{u}
	glol = !git gl @{u}..
	gli = !git fetch && git glil
	glo = !git fetch && git glol
	authors = log --format='%aN'
	authorsreverse = log --format='%aN' --reverse
	authorssimple = "!f() { git authorsreverse \"$@\" | awk '!visited[$0]++' | tac; }; f"
	find = "!f() { git log --all --full-history --stat -- \"**/${1:-.}\"; }; f"
	# hg
	cob = checkout -b
	ci = commit
	cia = commit -a
	addi = add --interactive
	cii = commit --interactive
	co = checkout
	heads = show-ref --heads
	incoming = !git fetch && git log --left-right ..@{u}
	outgoing = !git fetch && git log --left-right @{u}..
	paths = remote -v
	st = status --short
	stno = !git st -uno
	stall = !git st -uall
	stc = "!f() { git st \"$@\" | cut -c 4-; }; f"
	stnoc = "!f() { git stno \"$@\" | cut -c 4-; }; f"
	stallc = "!f() { git stall \"$@\" | cut -c 4-; }; f"
	cst = !git -c color.status=always status --short
	purge = clean -fd
	purgeall = !git clean -fd $(git root)
	cl = config --list --show-origin
	clg = "!f() { git cl | grep ${1:-.} | sed \"s,;.*,,\"; }; f"
	schemes = !git showconfig | grep ^url
	manifest = ls-tree -r --name-only --full-tree HEAD
	# merge
	me = merge
	mt = mergetool -y
	mea = merge --abort
	# misc
	addp = add --patch
	addu = add -u .
	addN = add -N
	rsho = remote set-head origin
	recommit = !git commit --allow-empty-message -t uncommit.txt
	recommita = !git commit --allow-empty-message -t uncommit.txt -a
	grepall = "!f() {  git grep \"$1\" $(git rev-list --all -- \"${2:-.}\") -- \"${2:-.}\"; }; f"
	firstco = !git checkout $(git firsthash)
	# push
	pushuu  = push --set-upstream
	pushu = !git pushuu origin
	pushdd = push --delete
	pushd = !git pushdd origin
	rea = rebase --abort
	rec = rebase --continue
	# reset
	rhard = reset --hard
	rmixed = reset --mixed
	rsoft = reset --soft
	# strip
	forcestripbare = update-ref HEAD HEAD^
	forcestrip = "!f() { git reset --hard $1^; }; f"
	dangling = fsck --no-reflogs
	cleanup = reflog expire --expire=now --all
	# shallow
	cloneshallow = clone --depth 1
	grafted = "!f() { git log --oneline --format=%D | grep -q ^grafted; }; f"
	fetchd = "!f() { git fetch --no-tags --depth ${1:-1}; }; f"
	rbranch = reset --hard @{u}
	gcprune = gc --prune=now
	# stash
	stlist = stash list
	stshow = stash show
	# submodule
	smflags = !git vartrue smquiet && echo --quiet
	smforeachflags = !git vartrue smrecursive && echo --recursive
	clonesub = clone --recurse-submodules --remote-submodules
	sm = submodule
	sminit = !git smg init
	smupdate = !git sm update --init
	smshallow = !git smupdate --depth 1
	smdeinit = !git sm deinit
	smdeinitall = !git smdeinit --all
	smfor = !git sm foreach
	sma = !git sm add
	sms = !git sm summary
	disub = diff --submodule
	subdiff = diff --submodule=diff
	nl2nul = !tr \"\\n\" \"\\0\"
	lssubmodules = !git ls-files --stage | grep 160000 | cut -c 51-
	lssubmodules0 = !git lssubmodules | git nl2nul
	addsubmodules = !git lssubmodules0 | xargs -0 git add
	unaddsubmodules = !git lssubmodules0 | xargs -0 git reset --
	catsubmodules = config -f .gitmodules -l
	# pullrebase
	rebcont = rebase --continue
	rebskip = rebase --skip
	pullmaster = !git pullbranch master
